import json
from webScraper import save_dict_to_csv
from urllib.parse import urlparse


def read_json(file, result_dict):
    with open(file, encoding="utf8") as json_file:
        data = json.load(json_file)
        num_results = 0
        for current_item in data:
            string_result = ""
            num_results += len(current_item["organicResults"])
            for organicResult in current_item["organicResults"]:
                if organicResult["url"] in result_dict.values():
                    print(f"{organicResult['url']} ya existe!!!\n")
                else:
                    string_result += f'{organicResult["title"]} ({organicResult["url"]}) - {organicResult["description"]}\n'
                    result_dict['title'].append(organicResult["title"])
                    result_dict['url'].append(organicResult["url"])
                    result_dict['host'].append('{uri.scheme}://{uri.netloc}/'.format(uri=urlparse(organicResult["url"])))
                    result_dict['description'].append(organicResult["description"])
                    result_dict['url_request'].append(current_item["url"])
                    result_dict['search_query'].append(current_item["searchQuery"]["term"])

        print(f'Query: [{data[0]["searchQuery"]["term"]}] {data[0]["url"]}\n'
              f'Results: {data[0]["resultsTotal"]} - Results scraping: {num_results}\n')


if __name__ == '__main__':
    filenames = ['blockchain data privacy - 5 pags.json', 'blockchain data storage - 5 pags.json',
                 'blockchain governance - 5 pags.json', 'blockchain latency - 5 pags.json',
                 'blockchain throughput - 5 pags.json', 'data DLT privacy - 5 pags.json',
                 'data DLT storage - 5 pags.json', 'DLT governance - 5 pags.json',
                 'DLT latency - 5 pags.json', 'DLT throughput - 5 pags.json', ]
    result_dict = {'search_query': [], 'title': [], 'url': [], 'host': [], 'description': [], 'url_request': []}
    for filename in filenames:
        file = f'.\data\grey_literature\{filename}'
        read_json(file, result_dict)
    save_dict_to_csv(result_dict)