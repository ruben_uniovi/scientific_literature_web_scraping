import requests
from urllib.error import HTTPError, URLError
from bs4 import BeautifulSoup
import bs4
import pandas
import datetime
from pathlib import Path


MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
          'November', 'December']
CATEGORIES = ['research-article']


def get_acm(url):
    url = 'https://dl.acm.org/action/doSearch?fillQuickSearch=false&expand=dl&AfterMonth=1&AfterYear=2007' \
          '&AllField=Title%3A%28%28%28Blockchain%3F+OR+DLT%3F%29++AND+%28%28data+AND%28privacy+OR+%28storage' \
          '%29%29%29%29+AND+Title%3A%28%28blockchain%3F+or+DLT%3F%29+and+%28governance+or+latency+or+throughput%29%29' \
          '&startPage=0&pageSize=50'
    result_dict = {'title': [], 'category': [], 'DOI': [], 'date': [], 'authors': [], 'abstract': []}
    try:
        html = requests.get(url)
        # save_html_to_file('acm_response_first_page.html', url)

    except HTTPError as e:
        print(e)
    except URLError:
        print('The server could not be found!')
    else:
        bs = BeautifulSoup(html.content, 'html.parser')
        total_items = int(bs.find("span", class_="hitsLength").get_text().replace(',', ''))
        total_pages = int(total_items / 50 if total_items % 50 == 0 else total_items / 50 + 1)
        print(f'Total items: {total_items}. Total pages: {total_pages}')
        all_items = bs.findAll('li', {'class': 'search__item issue-item-container'})
        try:
            count = 0
            for item in all_items:
                authors = []
                if item.find('div', class_='issue-heading').get_text() in CATEGORIES:
                    item_detail = item.find("span", class_='dot-separator')
                    for detail in item_detail:
                        if type(detail) == bs4.element.Tag:
                            if detail.get_text()[:-7] in MONTHS:
                                date = detail.get_text()[:-2]
                    for author in item.find('ul', 'rlist--inline loa truncate-list').findAll('a'):
                        authors.append(author.get('title'))
                    result_dict['title'].append(item.find("h5", class_="issue-item__title").get_text())
                    result_dict['category'].append(item.find("div", class_="issue-heading").get_text())
                    result_dict['date'].append(date)
                    result_dict['authors'].append(authors)
                    result_dict['abstract'].append(item.find("div", class_="abstract-text").get_text()[8:])
                    doi_url = item.find("a", class_="issue-item__doi dot-separator")
                    result_dict['DOI'].append(doi_url.get_text() if doi_url is not None else None)
                    print(f'Title: {item.find("h5", class_="issue-item__title").get_text()} '
                          f'( {item.find("div", class_="issue-heading").get_text()} ) \n'
                          f'Date: {date}\n'
                          f'Authors: {authors}\n'
                          f'Abstract: {item.find("div", class_="abstract-text").get_text()[8:]}\n'
                          f'DOI: {doi_url.get_text() if doi_url is not None else None}\n')
                    count += 1
            print(f'Exported items Count: {count}/{len(all_items)}')
            save_dict_to_csv(result_dict)
        except Exception as e:
            print(f'Something went wrong in main: {e}\n')


def get_springer(result_dict, url):
    try:
        response = requests.get(url).json()

    except HTTPError as e:
        print(e)
    except URLError:
        print('The server could not be found!')
    else:
        print(f"\nTotal: {response['result'][0]['recordsDisplayed']}\n"
              f"Query: {response['query']}")
        for item in response['records']:
            authors = []
            keywords = []
            for author in item['creators']:
                authors.append(author['creator'])

            doi = item['identifier'][4:]
            full_article_page = requests.request("GET", f'http://api.springernature.com/meta/v2/json?q=doi:{doi}&api_key=aa8ac05ed35d3a008040ad7f866b68af').json()

            for facet in full_article_page['facets']:
                if facet['name'] == 'keyword':
                    for keyword in facet['values']:
                        keywords.append(keyword['value'])
            '''
            print(f"Type: {item['contentType']}\n"
                  f"DOI: {doi}\n"
                  f"Title: {item['title']}\n"
                  f"Date: {item['publicationDate']}\n"
                  f"Authors:{authors}\n"
                  f"Abstract: {item['abstract']}")
            '''
            result_dict['title'].append(item['title'])
            result_dict['category'].append(item['contentType'])
            result_dict['DOI'].append(doi)
            result_dict['date'].append(item['publicationDate'])
            result_dict['authors'].append(authors)
            result_dict['abstract'].append(item['abstract'])
            result_dict['keywords'].append(keywords if len(keywords) > 0 else None)
            print(f"Añado: {item['title']}")


def get_science_direct(url):
    try:
        payload = {}
        headers = {
            'Cookie': 'sd_search_atgroup=B; sd_search=eyJhdXRoSGlzdG9yeSI6eyJwcmV2aW91c0N1c3RvbWVyIjpmYWxzZSwicHJldml'
                      'vdXNMb2dJbiI6ZmFsc2V9fQ%3D%3D.QuriVmdE0iYbFirfLWTT7%2BLc9cjMl%2FZu6OhMURqaOfk; sd_search_at='
                      'session%232368944480ef4577aaff5263d7c103e0%231588413723%7CPC%23f562d97143f04f5985204d9ea5b9af6e.'
                      '37_0%231651656663; __cfduid=d0c22478c1a9373ba24923cc3125921ae1588258483; '
                      'EUID=cad12a52-88d8-4e67-b159-e0f5b2fc4a3b; id_ab=C:5:7:805b0246-79d4-4d68-82ad-fe666e2bc8f2; '
                      'sd_session_id=f0fc98f654c26-4b67-afb2-3c3372f2c91egxrqb; '
                      'acw=f0fc98f654c26-4b67-afb2-3c3372f2c91egxrqb%7C%24%7C48C96E1E0E22D26FAB690E80DBC10731E614F7F30'
                      '7AACFF0E25458A0FD8231807AD75FD87057F029C51031EB8F67A1B6A267D4D875B591F0E9169905BBD791CB0469A6759'
                      '7464825D387A21AFA2E514; MIAMISESSION=3ef30222-a2a4-46de-ac25-63ae64dfcb44:3765864662'
        }

        response = requests.request("GET", url, headers=headers, data=payload)

        print(response)

    except HTTPError as e:
        print(e)
    except URLError:
        print('The server could not be found!')
    else:
        print('aqui')
        bs = BeautifulSoup(response.content, 'html.parser')


def get_science_direct_from_file(result_dict, file):
    with open(file, "r", encoding="utf-8") as f:
        contents = f.read()

        bs = BeautifulSoup(contents, 'html.parser')

        all_items = bs.findAll('li', {'class': 'ResultItem col-xs-24 push-m'})

        print(f"Total items: {len(all_items)}")
        for item in all_items:
            # item = all_items[0]
            title = item.find('a', {'class': 'result-list-title-link u-font-serif text-s'}, href=True)
            authors_list = list(map(lambda author: author.get_text(), item.findAll('span', {'class': 'author'})))
            subtitle = item.find('div', {'class': 'SubType hor'}).findAll('span')
            publication_name = subtitle[0].get_text()
            doi = item['data-doi']
            url = f"https://api.elsevier.com/content/article/doi/{doi}?apiKey=f1a2af971f6a1a6cb09e192507aff1d1&httpAccept=application%2Fjson"
            response = requests.request("GET", url).json()
            article_data = response['full-text-retrieval-response']['coredata']
            abstract = clean_abstract_science_direct(article_data['dc:description'])
            if article_data.get('dcterms:subject', None) is None:
                keywords_list = []
            else:
                keywords_list = list(map(lambda x: x['$'], article_data['dcterms:subject']))
            date = article_data['prism:coverDate']
            print(f"Title: {title.get_text()}\n"
                  f"DOI: {doi}\n"
                  f"URL: https://www.sciencedirect.com/{title['href']}\n"
                  f"Authors: {authors_list}\n"
                  f"Date: {date}\n"
                  f"Publication name: {publication_name}\n"
                  f"Abstract: {abstract}\n"
                  f"Keywords: {keywords_list}"
                  )
            result_dict['title'].append(title.get_text())
            result_dict['publication'].append(publication_name)
            result_dict['DOI'].append(doi)
            result_dict['date'].append(date)
            result_dict['authors'].append(authors_list)
            result_dict['abstract'].append(abstract)
            result_dict['keywords'].append(keywords_list)
            result_dict['url'].append(f"https://www.sciencedirect.com/{title['href']}")
            print("----------------------------------------------------")


def clean_abstract_science_direct(abstract):
    abs_result = abstract.replace('\n', '').replace('Abstract ', '')
    return ' '.join([x for x in abs_result.split(' ') if len(x) > 2])


def save_html_to_file(filename_with_extension, url):
    with open(filename_with_extension, 'w') as f_out:
        html = requests.get(url)
        soup = BeautifulSoup(html.content, 'html.parser')
        f_out.write(soup.prettify())


def save_dict_to_csv(dictionary):
    time = datetime.datetime.now().strftime("%d-%m-%Y_%H-%M")
    csv_name = f'{time}_results.csv'
    try:
        Path('./exports/').mkdir(parents=True, exist_ok=True)
        pandas.DataFrame(dictionary).to_csv('./exports/' + csv_name, encoding='utf-8', index=False)
        print(f'{csv_name} generated successfully!')
    except Exception as e:
        print(f'Something went wrong in save_dict_to_csv: {e}')


if __name__ == '__main__':
    # get_acm('url')
    result = {'title': [], 'category': [], 'DOI': [], 'date': [], 'authors': [], 'abstract': [], 'keywords': []}
    result_science_direct = {'title': [], 'publication': [], 'DOI': [], 'date': [], 'authors': [], 'abstract': [],
                             'keywords': [], 'url': []}

    springer_inputs = {'"data storage" AND title:blockchain', '"data storage" AND title:blockchain&s=11',
                       '"data privacy" AND title:blockchain', 'governance AND title:blockchain',
                       'latency AND title:blockchain', 'throughput AND title:blockchain',
                       '"data privacy" AND title:dlt', '"data storage" AND title:dlt',
                       'governance AND title:dlt', 'latency AND title:dlt',
                       'throughput AND title:dlt'}

    for springer_input in springer_inputs:
        get_springer(result,
                     f'http://api.springernature.com/meta/v2/json?api_key=aa8ac05ed35d3a008040ad7f866b68af&q=title:'
                     f'{springer_input}')
    save_dict_to_csv(result)

    get_science_direct_from_file(result_science_direct, 'sources/sciencedirect.html')
    get_science_direct_from_file(result_science_direct, 'sources/sciencedirect-pag2.html')
    save_dict_to_csv(result_science_direct)

